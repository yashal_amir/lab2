#include <iostream>
#include <string>
int main()
{
  std::string phone;
  std::cout << "Please enter the phone number:";
  std::cin >> phone;
  std::cout << "(" << phone.substr(0,3) <<") " << phone.substr(3,4) << " " << phone.substr(7,4) << std::endl;
  return 0;
}
