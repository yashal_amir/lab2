#include <iostream>

int main()
{
  int price,tipPercentage;
  double tip;
  std::cout << "Please enter the price:";
  std::cin >> price;
  std::cout << "Please enter the tip percentage:";
  std::cin >> tipPercentage;
  tip = ((double) price*tipPercentage)/100.0;
  std::cout << "The tip is L" << tip  << std::endl;
  std::cout << "The total amount to pay is L" <<  (double) price + tip  << std::endl;
  return 0;
}
