
#include <iostream>
#include <cmath>

int main()
{
  int x,y;
  std::cout << "Please enter two numbers:";
  std::cin >> x >>y;
  std::cout << "The sum of " << x << " and " << y << " is: " << x + y << std::endl;
  return 0;
}
